# CRUD app with Apollo, GraphQL, NodeJs, Express, MongoDB &amp; React

Apps using GraphQL are fast and stable because they control the data they get, not the server.

GraphQL is a data query language developed internally by Facebook in 2012 before being publicly released in 2015. It provides an alternative 
to REST and ad-hoc web service architectures. It allows clients to define the structure of the data required, and exactly the same structure 
of the data is returned from the server. It is a strongly typed runtime which allows clients to dictate what data is needed. This avoids both 
the problems of over-fetching as well as under-fetching of data.

GraphQL is a query language for APIs and a runtime for fulfilling those queries with your existing data. GraphQL provides a complete and 
understandable description of the data in your API. Send a GraphQL query to your API and get exactly what you need, nothing more and 
nothing less. GraphQL queries always return predictable results. 

Learn more about from GraphQL official website — http://graphql.org/learn/